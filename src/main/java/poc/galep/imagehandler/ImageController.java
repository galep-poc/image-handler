package poc.galep.imagehandler;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import poc.galep.imagehandler.utils.FileUploadUtils;

import java.io.IOException;
import java.util.UUID;

@RestController
public class ImageController {

    private static final String DIRECTORY = "/tmp/image-handler/";

    @PostMapping("/images")
    public void postImage(@RequestParam("image") MultipartFile multiPartFile) throws IOException {
        String fileName = UUID.randomUUID() + ".jpg";
        String uploadDirectory = DIRECTORY;

        FileUploadUtils.saveFile(uploadDirectory, fileName, multiPartFile);
    }
}
